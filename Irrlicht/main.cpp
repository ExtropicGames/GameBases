#include <assert.h>
#include <irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

int main(int argc, char* argv[]) {
    IrrlichtDevice* device = createDevice(EDT_OPENGL, dimension2d<u32>(640, 480), 16, false, false, false, 0);

    assert(device);


    // get pointers to things

    IVideoDriver* driver = device->getVideoDriver();
    ISceneManager* scene = device->getSceneManager();
    IGUIEnvironment* gui = device->getGUIEnvironment();

    // add some text and set window captions
    device->setWindowCaption(L"Irrlicht Engine Base");
    gui->addStaticText(L"Test of Irrlicht Software Renderer", rect<s32>(10,10,260,22), true);

    // load mesh and texture
    IAnimatedMesh* mesh = scene->getMesh("data/sample.md2");

    if (!mesh) {
        device->drop();
        return 1;
    }
    IAnimatedMeshSceneNode* node = scene->addAnimatedMeshSceneNode(mesh);

    if (node) {
        node->setMaterialFlag(EMF_LIGHTING, false);
        node->setMD2Animation(EMAT_STAND);
        node->setMaterialTexture(0, driver->getTexture("data/sample.bmp") );
    }

    // set up camera
    scene->addCameraSceneNode(0, vector3df(0, 30, -40), vector3df(0, 5, 0));

    // main game loop
    while (device->run()) {
        driver->beginScene(true, true, SColor(255, 100, 101, 140));
        scene->drawAll();
        gui->drawAll();
        driver->endScene();
    }

    // cleanup
    device->drop();
    return 0;
}
