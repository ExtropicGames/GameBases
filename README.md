GameBases
=========

A starting point for various games, providing makefiles and build configurations for many different platforms.

All original code is licensed under AGPLv3.
