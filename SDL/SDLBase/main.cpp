#include <iostream>

#include <cassert>

#ifdef WIN32
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>
#endif

#include "StateMachine.h"
#include "MainState.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

using namespace std;

int main(int argc, char** argv) {

    atexit(SDL_Quit);
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
        cout << "Could not init SDL." << endl;
        exit(1);
    }

    SDL_Surface* screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE|SDL_DOUBLEBUF);

    assert(screen);

    atexit(Mix_CloseAudio);
    // Set up SDL_mixer
    if (Mix_OpenAudio(22050, AUDIO_S16, 2, 4096)) {
        cout << "Could not init SDL_mixer." << endl;
        exit(1);
    }

    SDL_WM_SetCaption("SDLBase", NULL);

    SDL_EnableKeyRepeat(200, 10);

    StateMachine sm;
    sm.add(make_shared<QuitState>());
    sm.add(make_shared<MainState>());

    sm.start(1);

    while (!sm.quit()) {
        sm.update();
        sm.render();
    }

    return 0;
}
