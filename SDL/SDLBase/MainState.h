#ifndef MAIN_STATE_H
#define MAIN_STATE_H

#ifdef WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#endif

#include "StateMachine.h"
#include "GameLogic.h"

const int FRAMES_PER_SECOND = 60;


class MainState : public State {
public:
    int id() { return 1; }

    void start();
    void finish();
    void update();
    void render();
private:
    GameLogic game;

    SDL_Surface* screen;
};

#endif
