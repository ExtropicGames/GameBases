#include <cassert>

#include "sdlfunc.h"

SDL_Surface* loadImage(const char* filename) {
    SDL_Surface* unopt = IMG_Load(filename);
    assert(unopt);
    SDL_Surface* result = SDL_DisplayFormatAlpha(unopt);
    assert(result);
    SDL_FreeSurface(unopt);
    return result;
}

void paletteSwap(SDL_Surface* surface, SDL_Color* c1, SDL_Color* c2) {
    SDL_LockSurface(surface);
    
    Uint32 c1_pixel = SDL_MapRGBA(surface->format, c1->r, c1->g, c1->b, 255);
    Uint32 c2_pixel = SDL_MapRGBA(surface->format, c2->r, c2->g, c2->b, 255);
    
    Uint32* surface_pixels = (Uint32*) surface->pixels;
    
    for (int i = 0; i < surface->w * surface->h; i++) {
        if (surface_pixels[i] == c1_pixel) {
            surface_pixels[i] = c2_pixel;
        }
    }
    SDL_UnlockSurface(surface);
}
