#include <fstream>
#include <iostream>

#include "MainState.h"
#include "sdlfunc.h"

using namespace std;

void MainState::start() {

    screen = SDL_GetVideoSurface();

    // load images
    // SDL_Surface* image = loadImage("data/image.png");

    // read saved data
    // ifstream save_file("saves/file1.sav");
    // if (save_file.good()) {
    //     save_file.read((char*) &data, sizeof(int));
    // } else {
    //     data = default;
    // }
    // save_file.close();

    // start playing music
    // Mix_Music* music = Mix_LoadMUS("data/music/whatever.mp3");
    // Mix_PlayMusic(music, -1);

    // reset game logic
    game.reset();
}

void MainState::finish() {
    // write save data
    // ofstream save_file("saves/whatever.sav");
    // if (save_file.good()) {
    //     save_file.write((char*) &data, sizeof(int));
    // }
    // save_file.close();

    // stop music
    Mix_HaltMusic();

    // free resources
    // SDL_FreeSurface(image);
    // Mix_FreeMusic(music);

}

void MainState::update() {

    SDL_Event event;

    // handle events
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                sm->transition(0);
                return;
            }
        } else if (event.type == SDL_QUIT) {
            sm->transition(0);
            return;
        }
    }

    // do game logic update
}

void MainState::render() {

    // SDL_BlitSurface(background, NULL, screen, NULL);

    SDL_Flip(screen);

    // Regulate frame rate.
    static Uint32 next_frame = 0;
    Uint32 now = SDL_GetTicks();
    if (next_frame <= now) {
        next_frame = now + (1000 / FRAMES_PER_SECOND);
    } else {
        SDL_Delay(next_frame - now);
    }
}
