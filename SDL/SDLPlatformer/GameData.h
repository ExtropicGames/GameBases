#ifndef GAME_DATA_H
#define GAME_DATA_H

#include <list>

#include "Map.h"
#include "Actor.h"

class Actor;

class GameData {
public:
    std::shared_ptr<Map> map;

    // actors
    std::shared_ptr<Actor> player;
    std::list<std::shared_ptr<Actor>> actors;
};

#endif