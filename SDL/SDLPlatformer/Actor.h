#ifndef ACTOR_H
#define ACTOR_H

#include <string>

#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

#include "GameData.h"

enum Action { 
    NO_ACTION,
	RUN_LEFT, 
	RUN_RIGHT, 
	JUMP,
	FALL 
};

class Actor {
public:
	// public methods
	Actor(std::string name);

	void update(std::shared_ptr<GameData> game, Action action);
	void render(SDL_Surface* screen);
	bool collides(Actor &other);

	// public member data
	int x = 0;
	int y = 0;
private:
	SDL_Surface* image;
};

#endif