#include <iostream>

#include "sdlfunc.h"
#include "Map.h"

using namespace std;

Map::Map(string name) {
	map_image = loadImage(("data/maps/" + name + ".png").c_str());
	map_collision = loadImage(("data/maps/" + name + "_collision.png").c_str());
}

void Map::render(SDL_Surface* screen) {
	//TODO: translate this properly based on the camera position
	SDL_BlitSurface(map_image, NULL, screen, NULL);
}

bool Map::collides(SDL_Rect area) {
	// TODO: this algorithm just checks the four corners of the area. it should instead
	// check that the value of the collision mask within the entire area is 0.
	
    Uint32 pixel = getpixel(map_collision, area.x + area.w, area.y + area.h);
	if (pixel <= 255) {
        return true;
    }
    
    if (getpixel(map_collision, area.x, area.y + area.h) <= 255) {
        return true;
    }
    
    if (getpixel(map_collision, area.x + area.w, area.y) <= 255) {
        return true;
    }
    
    if (getpixel(map_collision, area.x, area.y) <= 255) {
        return true;
    }
    
	return false;
}