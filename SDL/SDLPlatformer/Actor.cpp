#include <iostream>

#include "Actor.h"
#include "sdlfunc.h"

using namespace std;

Actor::Actor(string name) {
	image = loadImage(("data/characters/" + name + ".png").c_str());
}

void Actor::update(shared_ptr<GameData> game, Action action) {

    SDL_Rect me = { (Sint16) x, (Sint16) y, 64, 64};

	if (action == RUN_LEFT) {
		x--;
		if (game->map->collides(me)) {
            x++;
	    }
	} else if (action == RUN_RIGHT) {
		x++;
		if (game->map->collides(me)) {
            x--;
	    }
	} else if (action == JUMP) {
		y -= 2;
		if (game->map->collides(me)) {
            y += 2;
	    }
	} else if (action == FALL) {
	    y++;
	    // TODO: check other actors too
        SDL_Rect me = { (Sint16) x, (Sint16) y, 64, 64};
        if (game->map->collides(me)) {
            y--;
        }
		
	}
}

void Actor::render(SDL_Surface* screen) {
	SDL_Rect where = { (Sint16) x, (Sint16) y, 32, 32};
	SDL_BlitSurface(image, NULL, screen, &where);
}

bool Actor::collides(Actor &other) {
	// TODO: check collision
	return false;
}