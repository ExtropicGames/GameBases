#ifndef MAIN_STATE_H
#define MAIN_STATE_H

#include <list>

#ifdef WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#endif

#include "StateMachine.h"
#include "Actor.h"
#include "Map.h"

const int FRAMES_PER_SECOND = 60;

class MainState : public State {
public:
    int id() { return 1; }

    void start();
    void finish();
    void update();
    void render();
private:
    SDL_Surface* screen;

    std::shared_ptr<GameData> game;
};

#endif
