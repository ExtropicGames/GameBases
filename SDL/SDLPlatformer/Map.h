#ifndef MAP_H
#define MAP_H

#include <string>

#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

class Map {
public:
	Map(std::string name);

	void update();
	void render(SDL_Surface* screen);

	bool collides(SDL_Rect area);
private:
	SDL_Surface* map_image;
	SDL_Surface* map_collision;	
};

#endif