#include <fstream>
#include <iostream>

#include "MainState.h"
#include "sdlfunc.h"

using namespace std;

void MainState::start() {

    screen = SDL_GetVideoSurface();

    game.reset(new GameData());

    // load images
    game->map.reset(new Map("default"));
    shared_ptr<Actor> player(new Actor("player"));
    game->actors.push_back(player);

    // read saved data
    // ifstream save_file("saves/file1.sav");
    // if (save_file.good()) {
    //     save_file.read((char*) &data, sizeof(int));
    // } else {
    //     data = default;
    // }
    // save_file.close();

    // start playing music
    // Mix_Music* music = Mix_LoadMUS("data/music/whatever.mp3");
    // Mix_PlayMusic(music, -1);
}

void MainState::finish() {
    // write save data
    // ofstream save_file("saves/whatever.sav");
    // if (save_file.good()) {
    //     save_file.write((char*) &data, sizeof(int));
    // }
    // save_file.close();

    // stop music
    Mix_HaltMusic();

    // free resources
    // Mix_FreeMusic(music);
}

void MainState::update() {

    Action action = NO_ACTION;

    SDL_Event event;

    // handle events, get the player action
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                sm->transition(0);
                return;
            } else if (event.key.keysym.sym == SDLK_LEFT) {
                action = RUN_LEFT;
            } else if (event.key.keysym.sym == SDLK_RIGHT) {
                action = RUN_RIGHT;
            } else if (event.key.keysym.sym == SDLK_SPACE) {
                action = JUMP;
            }
        } else if (event.type == SDL_QUIT) {
            sm->transition(0);
            return;
        }
    }

    game->actors.front()->update(game, action);

    for (shared_ptr<Actor> a : game->actors) {
        a->update(game, FALL);
    }

    // do game logic update
}

void MainState::render() {

    // draw the level
    game->map->render(screen);

    for (shared_ptr<Actor> a : game->actors) {
        a->render(screen);
    }

    SDL_Flip(screen);

    // Regulate frame rate.
    static Uint32 next_frame = 0;
    Uint32 now = SDL_GetTicks();
    if (next_frame <= now) {
        next_frame = now + (1000 / FRAMES_PER_SECOND);
    } else {
        SDL_Delay(next_frame - now);
    }
}
